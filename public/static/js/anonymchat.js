var socket = io();
$('#chat-form').submit(function() {
    if ($('#message').val() != "") {
        socket.emit('chat message', $('#message').val());

        $('#message').val('');
    }
    return false;
});

socket.on('chat message', function(msg) {
    $('#history').append($('<button type="button" class="btn btn-info btn-lg btn-block">').text(msg));
    var elem = document.getElementById('history');
    elem.scrollTop = elem.scrollHeight;
});
