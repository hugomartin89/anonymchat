var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
/*
app.get('/', function(req, res){
    res.sendFile('index.html');
});
*/

app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });
});

http.listen(8080, function(){
    console.log('Serve at localhost:8080');
});
